﻿using NUnit.Framework;

namespace PalindromeFinder.Tests
{
    public class AcceptanceTests
    {
        [Test]
        public void Scenario()
        {
             // Assign
            var searcher = new PalindromeTextFinder();
            const string input = "sqrrqabccbatudefggfedvwhijkllkjihxymnnmzpop";
            const string expected1 = "hijkllkjih";
            const string expected2 = "defggfed";
            const string expected3 = "abccba";

            // Act 
            var result = searcher.Find(input);

            // Assert
            Assert.AreEqual(3, result.Count);

            Assert.AreEqual(expected1, result[0].Text);
            Assert.AreEqual(expected1.Length, result[0].Length);
            Assert.AreEqual(input.IndexOf(expected1), result[0].StartIndex);

            Assert.AreEqual(expected2, result[1].Text);
            Assert.AreEqual(expected2.Length, result[1].Length);
            Assert.AreEqual(input.IndexOf(expected2), result[1].StartIndex);
            
            Assert.AreEqual(expected3, result[2].Text);
            Assert.AreEqual(expected3.Length, result[2].Length);
            Assert.AreEqual(input.IndexOf(expected3), result[2].StartIndex);
        }
    }
}
