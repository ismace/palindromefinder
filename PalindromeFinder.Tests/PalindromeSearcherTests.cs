﻿using System.Linq;
using NUnit.Framework;

namespace PalindromeFinder.Tests
{
    public class PalindromeSearcherTests
    {
        [Test]
        public void ShouldHandleExcpetionWhenInputNull()
        {
            // Assign
            var searcher = new PalindromeTextFinder();

            // Act // Assert 
            Assert.DoesNotThrow(() => searcher.Find(null));
        }

        [Test]
        public void ShouldReturnEmptyWhenInputEmpty()
        {
            // Assign
            var searcher = new PalindromeTextFinder();

            // Act 
            var result = searcher.Find(null);

            // Assert
            Assert.IsEmpty(result);
        }

        [Test]
        public void ShouldReturnEmptyWhenInputIsSingleChar()
        {
            // Assign
            var searcher = new PalindromeTextFinder();

            // Act 
            var result = searcher.Find("x");

            // Assert
            Assert.IsEmpty(result);
        }

        [TestCase("aa", "aa")]
        [TestCase("bb", "bb")]
        [TestCase("Zxx", "xx")]
        public void ShouldFindPalindromeWhenInputHasTwoSameChars(string input, string expected)
        {
            // Assign
            var searcher = new PalindromeTextFinder();

            // Act 
            var result = searcher.Find(input);

            // Assert
            Assert.AreEqual(expected, result.First().Text);
            Assert.AreEqual(expected.Length, result.First().Length);
            Assert.AreEqual(input.IndexOf(expected), result.First().StartIndex);
        }

        [Test]
        public void ShouldFindMultiplePalindromes()
        {
            // Assign
            var searcher = new PalindromeTextFinder();

            // Act 
            var result = searcher.Find("aabb");

            // Assert
            Assert.AreEqual(2, result.Count);

            Assert.AreEqual("aa", result[0].Text);
            Assert.AreEqual(2, result[0].Length);
            Assert.AreEqual(0, result[0].StartIndex);

            Assert.AreEqual("bb", result[1].Text);
            Assert.AreEqual(2, result[1].Length);
            Assert.AreEqual(2, result[1].StartIndex);
        }

        [TestCase("abba", "abba")]
        [TestCase("123abba", "abba")]
        [TestCase("abba123", "abba")]
        [TestCase("xabbax123", "xabbax")]
        [TestCase("123xabbax123", "xabbax")]
        public void ShouldFindPalindromWithMoreThan2Chars(string input, string expected)
        {
            // Assign
            var searcher = new PalindromeTextFinder();

            // Act 
            var result = searcher.Find(input);

            // Assert
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual(expected, result[0].Text);
        }

        [TestCase("AAAAAAAAAAAAAAAA", "AAAAAAAAAAAAAAAA")]
        [TestCase("AAAAAAAAAAAAAAAAA", "AAAAAAAAAAAAAAAAA")]
        public void ShouldFindUniquePalindromes(string input, string expected)
        {
            // Assign
            var searcher = new PalindromeTextFinder();

            // Act 
            var result = searcher.Find(input);

            // Assert
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual(expected, result[0].Text);
        }

        [TestCase("bxb", "bxb")]
        [TestCase("aaa", "aaa")]
        [TestCase("abxba", "abxba")]
        [TestCase("123abxba", "abxba")]
        [TestCase("abxba123", "abxba")]
        [TestCase("123abxba123", "abxba")]
        public void ShouldFindPalindromeWithOddChars(string input, string expected)
        {
            // Assign
            var searcher = new PalindromeTextFinder();

            // Act 
            var result = searcher.Find(input);

            // Assert
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual(expected, result[0].Text);
        }


        [TestCase("122abxba1221", "abxba", "1221", "22")]
        [TestCase("AAAAAAAXXAAAAAAAAAAAAXXX", "AAAAAAAXXAAAAAAA", "XXAAAAAAAAAAAAXX", "XXX")]
        [TestCase("AAAAAAAXXAAAAAAAAAAAAYYY", "AAAAAAAXXAAAAAAA", "AAAAAAAAAAAA", "YYY")]
        public void ShouldOrderPalindromesByLength(string input, string expected1, string expected2, string expected3)
        {
            // Assign
            var searcher = new PalindromeTextFinder();

            // Act 
            var result = searcher.Find(input);

            // Assert
            Assert.AreEqual(3, result.Count);
            Assert.AreEqual(expected1, result[0].Text);
            Assert.AreEqual(expected2, result[1].Text);
            Assert.AreEqual(expected3, result[2].Text);
        }
    }
}
