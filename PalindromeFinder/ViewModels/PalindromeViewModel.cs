﻿using PalindromeFinder.Models;

namespace PalindromeFinder.ViewModels
{
    public class PalindromeViewModel
    {
        public PalindromeViewModel(Palindrome palindrome)
        {
            Text = string.Format("Text: {0}, Index: {1}, Length: {2}", palindrome.Text, palindrome.StartIndex, palindrome.Length);
        }

        public string Text { get; private set; }
    }
}