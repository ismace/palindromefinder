﻿using Caliburn.Micro;

namespace PalindromeFinder.ViewModels
{
    public class ShellViewModel : PropertyChangedBase, IShellViewModel
    {
        private readonly IPalindromeTextFinder _palindromeTextFinder;

        public ShellViewModel(IPalindromeTextFinder palindromeTextFinder)
        {
            _palindromeTextFinder = palindromeTextFinder;
            Palindromes = new BindableCollection<PalindromeViewModel>();
        }

        public BindableCollection<PalindromeViewModel> Palindromes { get; private set; }

        public string InputString { get; set; }

        public void Find()
        {
            var palindromes = _palindromeTextFinder.Find(InputString);

            Palindromes.Clear();

            foreach (var palindrome in palindromes)
                Palindromes.Add(new PalindromeViewModel(palindrome));
        }
    }
}