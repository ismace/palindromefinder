namespace PalindromeFinder.Models
{
    public class Palindrome
    {
        public Palindrome(string text, int startIndex)
        {
            Text = text;
            StartIndex = startIndex;
        }

        public string Text { get; set; }
        public int StartIndex { get; set; }

        public int EndIndex
        {
            get { return StartIndex + Length; }
        }

        public int Length
        {
            get { return Text.Length; }
        }
    }
}