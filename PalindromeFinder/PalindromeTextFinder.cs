﻿using System.Collections.Generic;
using System.Linq;
using PalindromeFinder.Models;

namespace PalindromeFinder
{
    public class PalindromeTextFinder : IPalindromeTextFinder
    {
        /// <summary>
        /// Returns longest 3 palindromes for given string.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public List<Palindrome> Find(string input)
        {
            var palindromes = new List<Palindrome>();

            if (input == null)
                return palindromes;

            if (input.Length == 1)
                return palindromes;

            var palindromeCenters = FindPalindromeCenters(input);

            var expandedPalindromes = ExpandPalindromes(input, palindromeCenters);

            return RemoveOverlaps(expandedPalindromes).Take(3).ToList();
        }

        private static IEnumerable<Palindrome> FindPalindromeCenters(string input)
        {
            for (var i = 0; i < input.Length - 1; i++)
            {
                if (input[i] == input[i + 1])
                {
                    // For even numbered palindromes
                    yield return new Palindrome(input.Substring(i, 2), i);
                }
                if (i + 2 < input.Length && input[i] == input[i + 2])
                {
                    // For odd numbered palindromes
                    yield return new Palindrome(input.Substring(i, 3), i);
                }
            }
        }

        private static IEnumerable<Palindrome> ExpandPalindromes(string input, IEnumerable<Palindrome> palindromes)
        {
            foreach (var palindrome in palindromes)
            {
                var expandedPalindrome = palindrome;

                var searchIndex = palindrome.StartIndex - 1;

                while (searchIndex >= 0 &&
                       expandedPalindrome.StartIndex + expandedPalindrome.Length < input.Length)
                {
                    if (input[searchIndex] != input[searchIndex + palindrome.Length + 1])
                        break;

                    expandedPalindrome.Text = input.Substring(searchIndex, palindrome.Length + 2);
                    expandedPalindrome.StartIndex = searchIndex;
                    searchIndex--;
                }

                yield return expandedPalindrome;
            }
        }

        private static IEnumerable<Palindrome> RemoveOverlaps(IEnumerable<Palindrome> allPalindromes)
        {
            var candidatePalindromes = allPalindromes.OrderByDescending(p => p.Length);

            var palindromes = new List<Palindrome> { candidatePalindromes.First() };

            foreach (var candidatePalindrome in candidatePalindromes)
            {
                if (!CheckIfOverlapping(palindromes, candidatePalindrome))
                    palindromes.Add(candidatePalindrome);
            }

            return palindromes;
        }

        private static bool CheckIfOverlapping(IEnumerable<Palindrome> palindromes, Palindrome candidatePalindrome)
        {
            foreach (var p in palindromes)
            {
                if (p.StartIndex <= candidatePalindrome.StartIndex
                    && p.EndIndex >= candidatePalindrome.EndIndex)
                    return true;
            }
            return false;
        }
    }
}