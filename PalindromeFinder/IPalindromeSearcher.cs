﻿using System.Collections.Generic;
using PalindromeFinder.Models;

namespace PalindromeFinder
{
    public interface IPalindromeTextFinder
    {
        List<Palindrome> Find(string input);
    }
}